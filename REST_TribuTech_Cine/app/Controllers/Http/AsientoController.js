'use strict'

const Asiento = use('App/Models/Asiento')

class AsientoController {
  /**
   * Mostrar asientos disponibles por sala
   */
  async show({ params, response }) {
    const asientos = await Asiento.query().where('id_sala', params.id).fetch()
    if (!asientos) return response.status(404).send('No se encontró asientos para esta sala.')
    return asientos
  }
}

module.exports = AsientoController
