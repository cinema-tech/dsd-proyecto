﻿using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class Pelicula
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string titulo { get; set; }
        [DataMember]
        public string duracion { get; set; }
        [DataMember]
        public string genero { get; set; }
        [DataMember]
        public string clasificacion { get; set; }
        [DataMember]
        public string productor { get; set; }
        [DataMember]
        public string pais { get; set; }
        [DataMember]
        public string protagonistas { get; set; }
        [DataMember]
        public string estado { get; set; }
    }
}

