'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Compra extends Model {
  static get hidden() {
    return ['created_at', 'updated_at']
  }

  static get table() {
    return 'compra'
  }

  cliente() {
    return this.belongsTo('App/Models/Cliente')
  }
}

module.exports = Compra
