'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/Asiento', faker => {
  return {
    codigo: `AS${faker.natural({ min: 0, max: 150 })}`,
  }
})

Factory.blueprint('App/Models/Cartelera', faker => {
  return {
    fecha: faker.date({ year: 2019, month: 3, string: true, american: false }),
    hora_inicio: `${faker.hour({ twentyfour: true })}:${faker.minute()}:00`,
    hora_fin: `${faker.hour({ twentyfour: true })}:${faker.minute()}:00`,
    tipo_pelicula: faker.pickone(['Doblada', 'Subtitulada']),
    lenguaje: faker.pickone(['ES', 'ENG']),
  }
})

Factory.blueprint('App/Models/Cliente', faker => {
  return {
    dni: faker.natural({ numerals: 9 }),
    nombres: faker.name(),
    apellido_paterno: faker.pickone(['Lopez', 'Perez', 'Rodriguez', 'Tapia', 'Espinoza']),
    apellido_materno: faker.pickone(['Lopez', 'Perez', 'Rodriguez', 'Tapia', 'Espinoza']),
    fecha_nacimiento: faker.date({ year: 1990 }),
    email: faker.email(),
    telefono: faker.phone(),
    password: '123456',
  }
})

Factory.blueprint('App/Models/Pelicula', faker => {
  return {
    titulo: faker.word({ syllables: 4 }),
    duracion: '01:00:00',
    genero: faker.word({ syllables: 6 }),
    clasificacion: faker.word({ syllables: 6 }),
    productor: faker.name(),
    pais: faker.country(),
    protagonistas: faker.word({ syllables: 8 }),
    portada: 'http://url.pe',
  }
})
