'use strict'

const Pelicula = use('App/Models/Pelicula')

class PeliculaController {
  /**
   * Listar Peliculas
   */
  async index() {
    const peliculas = await Pelicula.all()
    if (peliculas.length === 0) return response.status(404).send('No existen peliculas disponibles.')
    return peliculas
  }
  /**
   * Mostrar Pelicula
   */
  async show({ params, response }) {
    const pelicula = await Pelicula.find(params.id)
    if (!pelicula) return response.status(404).send('No se encontró la película.')
    return pelicula
  }
  /**
   * Crear Pelicula
   */
  async store({ request, response }) {
    const body = request.only(['titulo', 'duracion', 'genero', 'clasificacion', 'productor', 'pais', 'protagonistas'])
    try {
      const nuevaPelicula = new Pelicula()
      nuevaPelicula.titulo = body.titulo
      nuevaPelicula.duracion = body.duracion
      nuevaPelicula.genero = body.genero
      nuevaPelicula.clasificacion = body.clasificacion
      nuevaPelicula.productor = body.productor
      nuevaPelicula.pais = body.pais
      nuevaPelicula.protagonistas = body.protagonistas
      nuevaPelicula.save()
      return response.status(201).json(nuevaPelicula)
    } catch (e) {
      console.log(e)
      return response.status(599).send('Ocurrió un problema al crear la pelicula.')
    }
  }
  /**
   * Actualizar Pelicula
   */
  async update({ params, request, response }) {
    const body = request.only(['titulo', 'duracion', 'genero', 'clasificacion', 'productor', 'pais', 'protagonistas'])
    try {
      const pelicula = await Pelicula.find(params.id)
      if (pelicula) {
        pelicula.titulo = body.titulo
        pelicula.duracion = body.duracion
        pelicula.genero = body.genero
        pelicula.clasificacion = body.clasificacion
        pelicula.productor = body.productor
        pelicula.pais = body.pais
        pelicula.protagonistas = body.protagonistas
        pelicula.save()

        return response.status(200).send(`Se actualizó la película con ID: ${params.id}`)
      } else {
        return response.status(404).send(`No se encontró la película con ID: ${params.id} para actualizarla.`)
      }
    } catch (e) {
      console.log(e)
      return response.status(599).send('Ocurrió un problema al actualizar la película.')
    }
  }

  /**
   * Eliminar Pelicula
   */
  async destroy({ params, response }) {
    const pelicula = await Pelicula.find(params.id)
    if (pelicula) {
      await pelicula.delete()
      return response.status(200).send(`Se eliminó la película con ID: ${params.id}`)
    } else {
      return response.status(404).send(`No existe la película con ID: ${params.id} para eliminarla.`)
    }
  }
}

module.exports = PeliculaController
