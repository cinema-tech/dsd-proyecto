﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceTribuTech.Test
{
    public class DuplicadoException
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
