'use strict'

const Cartelera = use('App/Models/Cartelera')
const Database = use('Database')
const Collect = use('collect.js')

class CarteleraController {
  /**
   * Listar Carteleras
   */
  async index() {
    const cartelera = await Cartelera.all()
    if (cartelera.length === 0) return response.status(404).send('No existen carteleras disponibles.')
    return cartelera
  }

  /**
   * Mostrar Cartelera
   */
  async show({ params, response }) {
    const query = await Database.table('cartelera as c')
      .select(
        'c.id',
        'c.fecha',
        'c.hora_inicio',
        'c.hora_fin',
        'c.tipo_pelicula',
        'c.lenguaje',
        'p.titulo',
        'p.portada',
        'p.duracion',
        's.nombre',
        's.codigo',
        's.id as id_sala'
      )
      .innerJoin('pelicula as p', 'c.id_pelicula', 'p.id')
      .innerJoin('sala as s', 'c.id_sala', 's.id')
      .innerJoin('tiposala as ts', 's.id_tipo_sala', 'ts.id')
      .where('p.id', params.id)

    const collect = Collect(query)

    const groupedWrapper = collect.map(elm => {
      return {
        hora_inicio: elm.hora_inicio,
        hora_fin: elm.hora_fin,
        nombre_sala: elm.nombre,
        codigo_sala: elm.codigo,
        id_sala: elm.id_sala,
      }
    })

    if (collect.length === 0) return response.status(404).send('No se encontró la cartelera.')

    return {
      id_cartelera: collect.pluck('id').first(),
      fecha_pelicula: collect.pluck('fecha').first(),
      titulo_pelicula: collect.pluck('titulo').first(),
      tipo_pelicula: collect.pluck('tipo_pelicula').first(),
      portada_pelicula: collect.pluck('portada').first(),
      lenguaje_pelicula: collect.pluck('lenguaje').first(),
      duracion_pelicula: collect.pluck('duracion').first(),
      funciones: groupedWrapper,
    }
  }

  /**
   * Crear Cartelera
   */
  async store({ request, response }) {
    const body = request.only([
      'fecha',
      'hora_inicio',
      'hora_inicio',
      'id_pelicula',
      'tipo_pelicula',
      'lenguaje',
      'id_sala',
    ])
    try {
      const nuevaCartelera = new Cartelera()
      nuevaCartelera.fecha = body.fecha
      nuevaCartelera.hora_inicio = body.hora_inicio
      nuevaCartelera.hora_inicio = body.hora_inicio
      nuevaCartelera.id_pelicula = body.id_pelicula
      nuevaCartelera.tipo_pelicula = body.tipo_pelicula
      nuevaCartelera.lenguaje = body.lenguaje
      nuevaCartelera.id_sala = body.id_sala
      return response.status(201).json(nuevaCartelera)
    } catch (e) {
      console.log(e)
      return response.status(599).send('Ocurrió un problema al crear la cartelera.')
    }
  }

  /**
   * Actualizar Cartelera
   */
  async update({ params, request, response }) {
    const body = request.only([
      'fecha',
      'hora_inicio',
      'hora_inicio',
      'id_pelicula',
      'tipo_pelicula',
      'lenguaje',
      'id_sala',
    ])
    try {
      const cartelera = await Cartelera.find(params.id)
      if (cartelera) {
        cartelera.fecha = body.fecha
        cartelera.hora_inicio = body.hora_inicio
        cartelera.hora_inicio = body.hora_inicio
        cartelera.id_pelicula = body.id_pelicula
        cartelera.tipo_pelicula = body.tipo_pelicula
        cartelera.lenguaje = body.lenguaje
        cartelera.id_sala = body.id_sala
        cartelera.save()

        return response.status(200).send(`Se actualizó la cartelera con ID: ${params.id}`)
      } else {
        return response.status(404).send(`No se encontró la cartelera con ID: ${params.id} para actualizarla.`)
      }
    } catch (e) {
      console.log(e)
      return response.status(599).send('Ocurrió un problema al actualizar la cartelera.')
    }
  }
}

module.exports = CarteleraController
