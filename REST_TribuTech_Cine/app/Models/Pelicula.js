'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pelicula extends Model {
  static get hidden() {
    return ['estado', 'created_at', 'updated_at']
  }

  static get table() {
    return 'pelicula'
  }
}

module.exports = Pelicula
