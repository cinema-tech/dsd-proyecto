'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Ticket extends Model {
  static get hidden() {
    return ['id', 'created_at', 'updated_at']
  }

  static get table() {
    return 'ticket'
  }

  cartelera() {
    return this.hasMany('App/Models/Cartelera')
  }

  asiento() {
    return this.hasMany('App/Models/Asiento')
  }

  compra() {
    return this.hasMany('App/Models/Compra')
  }
}

module.exports = Ticket
