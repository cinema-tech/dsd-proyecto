﻿using System;
using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class Cartelera
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public DateTime  fecha { get; set; }
        [DataMember]
        public DateTime hora_inicio { get; set; }
        [DataMember]
        public DateTime hora_fin { get; set; }
        [DataMember]
        public int id_pelicula { get; set; }
        [DataMember]
        public string tipo_pelicula { get; set; }
        [DataMember]
        public string lenguaje { get; set; }
        [DataMember]
        public int id_sala { get; set; }
    }
}