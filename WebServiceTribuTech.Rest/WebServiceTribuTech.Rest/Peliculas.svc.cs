﻿using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using WebServiceTribuTech.Rest.Domain;
using WebServiceTribuTech.Rest.Errores;
using WebServiceTribuTech.Rest.Persistence;

namespace WebServiceTribuTech.Rest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Peliculas" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Peliculas.svc or Peliculas.svc.cs at the Solution Explorer and start debugging.
    public class Peliculas : IPeliculas
    {
        private PeliculaDAO peliculaDAO = new PeliculaDAO();

        public Pelicula CrearPelicula(Pelicula peliculaACrear)
        {
            Pelicula peliculaExistente = peliculaDAO.Obtener(peliculaACrear.id);
            if (peliculaExistente != null)
            {
                throw new WebFaultException<PeliculaException>(new PeliculaException()
                {
                    Codigo = 101,
                    Descripcion = "Pelicula duplicada"
                }, HttpStatusCode.Conflict);
            }

            return peliculaDAO.Crear(peliculaACrear);
        }

        public Pelicula ObtenerPelicula(string id)
        {
            return peliculaDAO.Obtener(id);
        }

        public Pelicula ModificarPelicula(Pelicula peliculaAModificar)
        {
            Pelicula peliculaExistente = peliculaDAO.Obtener(peliculaAModificar.id);

            if (peliculaExistente != null)
            {
                throw new WebFaultException<PeliculaException>(new PeliculaException()
                {
                    Codigo = 102,
                    Descripcion = "Pelicula no existe"
                }, HttpStatusCode.Conflict);
            }

            return peliculaDAO.Modificar(peliculaAModificar);
        }

        public void EliminarPelicula(string id)
        {
            Pelicula peliculaExistente = peliculaDAO.Obtener(id);

            if (peliculaExistente.estado != "ND")
            {
                throw new WebFaultException<PeliculaException>(new PeliculaException()
                {
                    Codigo = 103,
                    Descripcion = "Pelicula no se pudo eliminar. Estado: Disponible "
                }, HttpStatusCode.Conflict);
            }

            peliculaDAO.Eliminar(id);
        }

        public List<Pelicula> ListarPeliculas()
        {
            return peliculaDAO.Listar();
        }

        public List<Pelicula> ListarPeliculasPorEstado(string estado)
        {
            Pelicula validaPelicula = peliculaDAO.ValidarPelicula(estado);
            if (validaPelicula.estado == "ND")
            {
                throw new WebFaultException<PeliculaException>(new PeliculaException()
                {
                    Codigo = 104,
                    Descripcion = "Pelicula No Disponible"
                }, HttpStatusCode.Conflict);
            }
            return peliculaDAO.ListarPeliculasPorEstado(estado);
        }
    }
}
