'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Sala extends Model {
  static get hidden() {
    return ['id', 'created_at', 'updated_at']
  }

  static get table() {
    return 'sala'
  }

  tipoSala() {
    return this.belongsTo('App/Models/TipoSala')
  }
}

module.exports = Sala
