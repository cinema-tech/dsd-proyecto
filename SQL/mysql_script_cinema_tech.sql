CREATE TABLE `Cliente` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`dni` INT(10) NOT NULL UNIQUE,
	`nombres` VARCHAR(100) NOT NULL,
	`apellido_paterno` VARCHAR(100) NOT NULL,
	`apellido_materno` VARCHAR(100) NOT NULL,
	`fecha_nacimiento` DATE NOT NULL,
	`email` VARCHAR(100) NOT NULL,
	`telefono` VARCHAR(50) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`estado` BOOLEAN NOT NULL DEFAULT '1',
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Pelicula` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`titulo` VARCHAR(255) NOT NULL,
	`duracion` TIME NOT NULL,
	`genero` VARCHAR(50) NOT NULL,
	`clasificacion` VARCHAR(100) NOT NULL,
	`productor` VARCHAR(50) NOT NULL,
	`pais` VARCHAR(20) NOT NULL,
	`protagonistas` VARCHAR(255) NOT NULL,
	`portada` TEXT NOT NULL,
	`estado` BOOLEAN NOT NULL DEFAULT '1',
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Sala` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(100) NOT NULL,
	`codigo` VARCHAR(20) NOT NULL,
	`id_tipo_sala` INT NOT NULL,
	`cantidad_asientos` INT(6) NOT NULL,
	`estado` BOOLEAN NOT NULL DEFAULT '1',
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Cartelera` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`fecha` DATE NOT NULL,
	`hora_inicio` TIME NOT NULL,
	`hora_fin` TIME NOT NULL,
	`id_pelicula` INT NOT NULL,
	`tipo_pelicula` VARCHAR(50) NOT NULL,
	`lenguaje` VARCHAR(20) NOT NULL,
	`id_sala` INT NOT NULL,
	`estado` BOOLEAN NOT NULL DEFAULT '1',
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `TipoSala` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(255) NOT NULL,
	`estado` BOOLEAN NOT NULL DEFAULT '1',
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Ticket` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`id_cartelera` INT NOT NULL,
	`id_asiento` INT NOT NULL,
	`id_compra` INT NOT NULL,
	`costo` DECIMAL NOT NULL,
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Asiento` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`codigo` VARCHAR(8) NOT NULL,
	`disponible` BOOLEAN NOT NULL DEFAULT '1',
	`id_sala` INT NOT NULL DEFAULT '1',
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Compra` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`total_tickets` DECIMAL NOT NULL,
	`total_costo` DECIMAL NOT NULL,
	`id_cliente` INT NOT NULL,
	`created_at` TIMESTAMP,
	`updated_at` TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Sala` ADD CONSTRAINT `Sala_fk0` FOREIGN KEY (`id_tipo_sala`) REFERENCES `TipoSala`(`id`);

ALTER TABLE `Cartelera` ADD CONSTRAINT `Cartelera_fk0` FOREIGN KEY (`id_pelicula`) REFERENCES `Pelicula`(`id`);

ALTER TABLE `Cartelera` ADD CONSTRAINT `Cartelera_fk1` FOREIGN KEY (`id_sala`) REFERENCES `Sala`(`id`);

ALTER TABLE `Ticket` ADD CONSTRAINT `Ticket_fk0` FOREIGN KEY (`id_cartelera`) REFERENCES `Cartelera`(`id`);

ALTER TABLE `Ticket` ADD CONSTRAINT `Ticket_fk1` FOREIGN KEY (`id_asiento`) REFERENCES `Asiento`(`id`);

ALTER TABLE `Ticket` ADD CONSTRAINT `Ticket_fk2` FOREIGN KEY (`id_compra`) REFERENCES `Compra`(`id`);

ALTER TABLE `Asiento` ADD CONSTRAINT `Asiento_fk0` FOREIGN KEY (`id_sala`) REFERENCES `Sala`(`id`);

ALTER TABLE `Compra` ADD CONSTRAINT `Compra_fk0` FOREIGN KEY (`id_cliente`) REFERENCES `Cliente`(`id`);

