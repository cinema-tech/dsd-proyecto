﻿using System;
using System.ServiceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WCFServicesTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test1CrearCliente()
        {
            ClienteWS.ClienteServiceClient proxy = new ClienteWS.ClienteServiceClient();
            ClienteWS.Cliente clienteCreado = proxy.CrearCliente(new ClienteWS.Cliente()
            { 
                Dni = 11223344,
                Nombres = "Elena",
                ApellidoPaterno = "Cruz",
                ApellidoMaterno = "Talavera",
                FechaNacimiento = new DateTime(1990-01-10),
                Email = "elena@email.com",
            });
            Assert.AreEqual(11223344, clienteCreado.Dni);
            Assert.AreEqual("Elena", clienteCreado.Nombres);
            Assert.AreEqual("Cruz", clienteCreado.ApellidoPaterno);
            Assert.AreEqual("Talavera", clienteCreado.ApellidoMaterno);
            Assert.AreEqual(1990-01-10, clienteCreado.FechaNacimiento);
        }

        [TestMethod]
        public void Test2CrearClienteRepetido()
        {
            ClienteWS.ClienteServiceClient proxy = new ClienteWS.ClienteServiceClient();
            try
            {
                ClienteWS.Cliente clienteCreado = proxy.CrearCliente(new ClienteWS.Cliente()
                {
                    Dni = 11223344,
                    Nombres = "Elena",
                    ApellidoPaterno = "Cruz",
                    ApellidoMaterno = "Talavera",
                    FechaNacimiento = new DateTime(1990 - 01 - 10),
                    Email = "elena@email.com",
                });
            } catch (FaultException<ClienteWS.ClienteErrores> error)
            {
                Assert.AreEqual("Error al intentar crear un cliente", error.Reason);
                Assert.AreEqual(error.Detail.Codigo, "101");
                Assert.AreEqual(error.Detail.Descripcion, "El cliente ya se encuentra registrado");
            }
        }
    }
}
