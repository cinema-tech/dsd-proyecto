'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Sala = use('App/Models/Sala')
const Asiento = use('App/Models/Asiento')

class DatabaseSeeder {
  async run() {
    try {
      await Factory.model('App/Models/Cliente').createMany(5)
      await Factory.model('App/Models/Pelicula').createMany(10)

      const sala2D = await Sala.find(1)
      const sala3D = await Sala.find(2)
      const salaCorp = await Sala.find(3)

      for (let i = 0; i < 100; i++) {
        const asientos2D = new Asiento()
        asientos2D.codigo = `A${i}`
        asientos2D.id_sala = sala2D.id

        await asientos2D.save()

        const asientos3D = new Asiento()
        asientos3D.codigo = `A${i}`
        asientos3D.id_sala = sala3D.id

        await asientos3D.save()

        const asientosCorp = new Asiento()
        asientosCorp.codigo = `A${i}`
        asientosCorp.id_sala = salaCorp.id

        await asientosCorp.save()
      }
    } catch (e) {
      console.log(e)
    }
  }
}

module.exports = DatabaseSeeder
