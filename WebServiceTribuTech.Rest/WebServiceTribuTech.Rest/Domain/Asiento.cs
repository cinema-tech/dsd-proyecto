﻿using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class Asiento
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string codigo { get; set; }
        [DataMember]
        public bool  disponible { get; set; }
        [DataMember]
        public int id_sala { get; set; }
       
    }
}