'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cartelera extends Model {
  static get hidden() {
    return ['created_at', 'updated_at']
  }

  static get table() {
    return 'cartelera'
  }

  pelicula() {
    return this.hasMany('App/Models/Pelicula')
  }

  sala() {
    return this.hasMany('App/Models/Sala')
  }
}

module.exports = Cartelera
