﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using WebServiceTribuTech.Rest.Domain;

namespace WebServiceTribuTech.Rest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPeliculas" in both code and config file together.
    [ServiceContract]
    public interface IPeliculas
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Peliculas", ResponseFormat = WebMessageFormat.Json)]
        Pelicula CrearPelicula(Pelicula peliculadACrear);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Peliculas/{id}", ResponseFormat = WebMessageFormat.Json)]
        Pelicula ObtenerPelicula(string id);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Peliculas", ResponseFormat = WebMessageFormat.Json)]
        Pelicula ModificarPelicula(Pelicula peliculaAModificar);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Peliculas/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarPelicula(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Peliculas", ResponseFormat = WebMessageFormat.Json)]
        List<Pelicula> ListarPeliculas();

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "Peliculas/{estado}", ResponseFormat = WebMessageFormat.Json)]
        //List<Pelicula> ListarPeliculasPorEstado(string estado);

    }
}
