const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersRegistered(() => {
  const Request = use('Adonis/Src/Request')
  const Response = use('Adonis/Src/Response')
  const HttpContext = use('Adonis/Src/HttpContext')

  Request.macro('setHeader', function (key, value) {
    this.headers()[key] = value
  })
})
