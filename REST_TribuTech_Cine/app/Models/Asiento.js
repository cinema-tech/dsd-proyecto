'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Asiento extends Model {
  static get hidden() {
    return ['created_at', 'updated_at']
  }

  static get table() {
    return 'asiento'
  }

  sala() {
    return this.belongsTo('App/Models/Sala')
  }
}

module.exports = Asiento
