﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF_TribuTech_Cine.Dominio;
using WCF_TribuTech_Cine.Errores;

namespace WCF_TribuTech_Cine
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IClienteService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IClienteService
    {
        [OperationContract]
        List<Cliente> ListarClientes();

        [FaultContract(typeof(ClienteErrores))]
        [OperationContract]
        Cliente CrearCliente(Cliente ClienteObj);

        [OperationContract]
        Cliente ObtenerCliente(int Dni);

        [OperationContract]
        Cliente ActualizarCliente(Cliente ClienteObj);

        [OperationContract]
        void EliminarCliente(int Dni);
    }
}
