﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using WebServiceTribuTech.Rest.Domain;

namespace WebServiceTribuTech.Rest.Persistence
{
    public class PeliculaDAO
    {
        private string CadenaConexion = "datasource=127.0.0.1;port=3306;username=root;password=;database=cinematech;";

        public Pelicula Crear(Pelicula peliculaACrear)
        {
            Pelicula peliculaCreada = null;
            string sql = "Insert into pelicula (id, titulo, duracion, genero, clasificacion, productor, pais, protagonistas, estado) " +
                "values(@id, @titulo, @duracion, @genero, @clasificacion, @productor, @pais, @protagonistas, @estado)";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new MySqlParameter("@id", peliculaACrear.id));
                    comando.Parameters.Add(new MySqlParameter("@titulo", peliculaACrear.titulo));
                    comando.Parameters.Add(new MySqlParameter("@duracion", peliculaACrear.duracion));
                    comando.Parameters.Add(new MySqlParameter("@genero", peliculaACrear.genero));
                    comando.Parameters.Add(new MySqlParameter("@clasificacion", peliculaACrear.clasificacion));
                    comando.Parameters.Add(new MySqlParameter("@productor", peliculaACrear.productor));
                    comando.Parameters.Add(new MySqlParameter("@pais", peliculaACrear.pais));
                    comando.Parameters.Add(new MySqlParameter("@protagonistas", peliculaACrear.protagonistas));
                    comando.Parameters.Add(new MySqlParameter("@estado", peliculaACrear.estado));
                    comando.ExecuteNonQuery();
                }
            }
            peliculaCreada = Obtener(peliculaACrear.id);
            return peliculaCreada;
        }

        public Pelicula Obtener(string id)
        {
            Pelicula peliculaEncontrada = null;
            string sql = "select * from pelicula where id = @id";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new MySqlParameter("@id", id));
                    using (MySqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            peliculaEncontrada = new Pelicula()
                            {
                                id = (string)resultado["id"],
                                titulo = (string)resultado["titulo"],
                                duracion = (string)resultado["duracion"],
                                genero = (string)resultado["genero"],
                                clasificacion = (string)resultado["clasificacion"],
                                productor = (string)resultado["productor"],
                                pais = (string)resultado["pais"],
                                protagonistas = (string)resultado["protagonistas"],
                                estado = (string)resultado["estado"]
                            };
                        }
                    }
                }

            }
            return peliculaEncontrada;
        }

        public Pelicula Modificar(Pelicula peliculaAModificar)
        {
            Pelicula peliculaModificada = null;
            string sql = "update pelicula set titulo = @titulo, duracion = @duracion, " +
                "genero = @genero, clasificacion = @clasificacion " +
                "productor = @productor, pais = @pais, protagonistas = @protagonistas, estado = @estado where id = @id";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new MySqlParameter("@titulo", peliculaAModificar.titulo));
                    comando.Parameters.Add(new MySqlParameter("@duracion", peliculaAModificar.duracion));
                    comando.Parameters.Add(new MySqlParameter("@genero", peliculaAModificar.genero));
                    comando.Parameters.Add(new MySqlParameter("@clasificacion", peliculaAModificar.clasificacion));
                    comando.Parameters.Add(new MySqlParameter("@productor", peliculaAModificar.productor));
                    comando.Parameters.Add(new MySqlParameter("@pais", peliculaAModificar.pais));
                    comando.Parameters.Add(new MySqlParameter("@protagonistas", peliculaAModificar.protagonistas));
                    comando.Parameters.Add(new MySqlParameter("@estado", peliculaAModificar.estado));
                    comando.ExecuteNonQuery();
                }
            }
            peliculaModificada = Obtener(peliculaAModificar.id);
            return peliculaModificada;
        }

        public void Eliminar(string id)
        {
            string sql = "delete from pelicula where id = @id";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new MySqlParameter("@id", id));
                    comando.ExecuteNonQuery();
                }
            }
        }

        public List<Pelicula> Listar()
        {
            List<Pelicula> peliculasEncontradas = new List<Pelicula>();
            Pelicula peliculaEncontrada = null;
            string sql = "select * from pelicula";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    using (MySqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            peliculaEncontrada = new Pelicula()
                            {
                                id = (string)resultado["id"],
                                titulo = (string)resultado["titulo"],
                                duracion = (string)resultado["duracion"],
                                genero = (string)resultado["genero"],
                                clasificacion = (string)resultado["clasificacion"],
                                productor = (string)resultado["productor"],
                                pais = (string)resultado["pais"],
                                protagonistas = (string)resultado["protagonistas"],
                                estado = (string)resultado["estado"]
                            };
                            peliculasEncontradas.Add(peliculaEncontrada);
                        }
                    }
                }

            }

            return peliculasEncontradas;
        }

        public List<Pelicula> ListarPeliculasPorEstado(string estado)
        {
            List<Pelicula> peliculasEncontradas = new List<Pelicula>();
            Pelicula peliculaEncontrada = null;
            string sql = "select * from pelicula where estado = @estado";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new MySqlParameter("@estado", estado));
                    using (MySqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            peliculaEncontrada = new Pelicula()
                            {
                                id = (string)resultado["id"],
                                titulo = (string)resultado["titulo"],
                                duracion = (string)resultado["duracion"],
                                genero = (string)resultado["genero"],
                                clasificacion = (string)resultado["clasificacion"],
                                productor = (string)resultado["productor"],
                                pais = (string)resultado["pais"],
                                protagonistas = (string)resultado["protagonistas"],
                                estado = (string)resultado["estado"]
                            };
                            peliculasEncontradas.Add(peliculaEncontrada);
                        }
                    }
                }

            }

            return peliculasEncontradas;
        }

        public Pelicula ValidarPelicula(string estado)
        {
            Pelicula peliculaEncontrada = null;
            string sql = "select * from pelicula where estado = @estado";
            using (MySqlConnection conexion = new MySqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (MySqlCommand comando = new MySqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new MySqlParameter("@estado", estado));
                    using (MySqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            peliculaEncontrada = new Pelicula()
                            {
                                estado = (string)resultado["estado"],
                            };
                        }
                    }
                }

            }
            return peliculaEncontrada;
        }
    }
}