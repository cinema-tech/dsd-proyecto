Create Database CinemaTech;

Use CinemaTech;

CREATE TABLE pelicula (
  id VARCHAR(11) NOT NULL,
  titulo VARCHAR(150) NOT NULL,
  duracion VARCHAR(255) NOT NULL,
  genero VARCHAR(255) NOT NULL,
  clasificacion VARCHAR(255) NOT NULL,
  productor VARCHAR(255) NOT NULL,
  pais VARCHAR(255) NOT NULL,
  protagonistas VARCHAR(255) NOT NULL,
  estado VARCHAR(255) NOT NULL
);

INSERT INTO pelicula (id, titulo, duracion, genero, clasificacion, productor, pais, protagonistas, estado) 
  VALUES ("P0001", 'La monia', '120 min', 'Terror', 'Mayores 14', 'James Jacks', 'Estados Unidos', "Stephen Sommers, Nina Wilcox Putnam", "D");
INSERT INTO pelicula (id, titulo, duracion, genero, clasificacion, productor, pais, protagonistas, estado) 
  VALUES ("P0002", 'The Terminator', '108 min', 'Ciencia Ficccion', 'Mayores 14', 'Gale Anne Hurd', 'Estados Unidos', "Arnold Schwarzenegger, Linda Hamilton, Michael Biehn", "ND");
INSERT INTO pelicula (id, titulo, duracion, genero, clasificacion, productor, pais, protagonistas, estado) 
  VALUES ("P0003", 'The Terminator', '140 min', 'Drama', 'Mayores 14', 'Brian Grazer', 'Estados Unidos', "Tom Hanks, Kevin Bacon, Bill Paxton", "D");