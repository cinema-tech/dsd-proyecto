﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceTribuTech.Test
{
    public class Pelicula
    {
        public string id { get; set; }
        public string titulo { get; set; }
        public string duracion { get; set; }
        public string genero { get; set; }
        public string clasificacion { get; set; }
        public string productor { get; set; }
        public string pais { get; set; }
        public string protagonistas { get; set; }
        public string estado { get; set; }
    }
}
