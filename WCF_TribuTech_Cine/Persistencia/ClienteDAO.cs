﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using WCF_TribuTech_Cine.Dominio;

namespace WCF_TribuTech_Cine.Persistencia
{
    public class ClienteDAO
    {
        private string dbConnection = "server=localhost; database=tributech_cine_db; user=root; password=root;";

        public List<Cliente> Index()
        {
            List<Cliente> listarClientes = new List<Cliente>();
            Cliente cliente = null;

            string query = "SELECT * FROM cliente";

            using (MySqlConnection connection = new MySqlConnection(dbConnection))
            {

                connection.Open();

                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    using (MySqlDataReader response = command.ExecuteReader())
                    {
                        while (response.Read())
                        {
                            cliente = new Cliente
                            {
                                Dni = (int)response["dni"],
                                Nombres = (string)response["nombres"],
                                ApellidoPaterno = (string)response["apellido_paterno"],
                                ApellidoMaterno = (string)response["apellido_materno"],
                                FechaNacimiento = (DateTime)response["fecha_nacimiento"],
                                Email = (string)response["email"]
                            };

                            listarClientes.Add(cliente);
                        }
                    }
                }

            }
            return listarClientes;
        }

        public Cliente Show(int Dni)
        {
            Cliente encontrarCliente = null;

            string query = "SELECT * FROM cliente WHERE dni = @Dni";

            using (MySqlConnection connection = new MySqlConnection(dbConnection))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.Add(new MySqlParameter("@Dni", Dni));

                    using (MySqlDataReader response = command.ExecuteReader())
                    {
                        if (response.Read())
                        {
                            encontrarCliente = new Cliente
                            {
                                Dni = (int)response["dni"],
                                Nombres = (string)response["nombres"],
                                ApellidoPaterno = (string)response["apellido_paterno"],
                                ApellidoMaterno = (string)response["apellido_materno"],
                                FechaNacimiento = (DateTime)response["fecha_nacimiento"],
                                Email = (string)response["email"]
                            };
                        }
                    }
                }
            }
            return encontrarCliente;
        }

        public Cliente Store(Cliente ClienteObj)
        {
            Cliente nuevoCliente = null;

            string query = "INSERT INTO cliente VALUES(@Dni, @Nombres, @ApellidoPaterno, @ApellidoMaterno, @FechaNacimiento, @Email)";

            using (MySqlConnection connection = new MySqlConnection(dbConnection))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.Add(new MySqlParameter("@Dni", nuevoCliente.Dni));
                    command.Parameters.Add(new MySqlParameter("@Nombres", nuevoCliente.Nombres));
                    command.Parameters.Add(new MySqlParameter("@ApellidoPaterno", nuevoCliente.ApellidoPaterno));
                    command.Parameters.Add(new MySqlParameter("@ApellidoMaterno", nuevoCliente.ApellidoMaterno));
                    command.Parameters.Add(new MySqlParameter("@FechaNacimiento", nuevoCliente.FechaNacimiento));
                    command.Parameters.Add(new MySqlParameter("@Email", nuevoCliente.Email));
                    command.ExecuteNonQuery();
                }
            }
            nuevoCliente = Show(ClienteObj.Dni);
            return nuevoCliente;
        }

        public Cliente Update(Cliente ClienteObj)
        {
            Cliente actualizarCliente = null;

            string query = "UPDATE cliente SET dni = @Dni, nombres = @Nombres, apellido_paterno = @ApellidoPaterno, apellido_materno = @ApellidoMaterno, fecha_nacimiento = @FechaNacimiento, email = @Email";

            using (MySqlConnection connection = new MySqlConnection(dbConnection))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.Add(new MySqlParameter("@Dni", actualizarCliente.Dni));
                    command.Parameters.Add(new MySqlParameter("@Nombres", actualizarCliente.Nombres));
                    command.Parameters.Add(new MySqlParameter("@ApellidoPaterno", actualizarCliente.ApellidoPaterno));
                    command.Parameters.Add(new MySqlParameter("@ApellidoMaterno", actualizarCliente.ApellidoMaterno));
                    command.Parameters.Add(new MySqlParameter("@FechaNacimiento", actualizarCliente.FechaNacimiento));
                    command.Parameters.Add(new MySqlParameter("@Email", actualizarCliente.Email));
                    command.ExecuteNonQuery();
                }
            }
            actualizarCliente = Show(ClienteObj.Dni);
            return actualizarCliente;
        }

        public void Delete(int Dni)
        {
            string query = "DELETE FROM cliente WHERE dni = @Dni";

            using (MySqlConnection connection = new MySqlConnection(dbConnection))
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.Add(new MySqlParameter("@Dni", Dni));
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}