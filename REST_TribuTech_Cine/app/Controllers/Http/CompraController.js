'use strict'

const Compra = use('App/Models/Compra')
const Ticket = use('App/Models/Ticket')

class CompraController {
  /**
   * Crear compra
   */
  async store({ request, response }) {
    const body = request.only([
      'total_tickets',
      'total_costo',
      'id_cliente',
      'id_cartelera',
      'id_asiento',
      'id_compra',
      'costo',
    ])
    try {
      const nuevaCompra = new Compra()
      nuevaCompra.total_tickets = body.total_tickets
      nuevaCompra.total_costo = body.total_costo
      nuevaCompra.id_cliente = body.id_cliente
      await nuevaCompra.save()

      const nuevoTicket = new Ticket()
      nuevoTicket.id_cartelera = body.id_cartelera
      nuevoTicket.id_asiento = body.id_asiento
      nuevoTicket.id_compra = nuevaCompra.id
      nuevoTicket.costo = body.costo
      await nuevoTicket.save()

      return response.status(201).json('Compra realizada.')
    } catch (e) {
      return response.status(599).send('Ocurrió un problema al crear la compra.')
    }
  }
}

module.exports = CompraController
