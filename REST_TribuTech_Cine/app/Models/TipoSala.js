'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TipoSala extends Model {
  static get hidden() {
    return ['id', 'created_at', 'updated_at']
  }

  static get table() {
    return 'tiposala'
  }
}

module.exports = TipoSala
