﻿using System;
using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class Cliente
    {

        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int dni { get; set; }
        [DataMember]
        public string nombres { get; set; }
        [DataMember]
        public string apellido_paterno { get; set; }
        [DataMember]
        public string apellido_materno { get; set; }
        [DataMember]
        public DateTime fecha_nacimiento { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public int telefono { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public bool estado { get; set; }
    }
}