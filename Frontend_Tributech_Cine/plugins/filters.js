import Vue from 'vue'
import dayjs from 'dayjs'

Vue.filter('formatDate', (date) => dayjs(date).format('DD/MM/YYYY'))
