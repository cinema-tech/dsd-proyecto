﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF_TribuTech_Cine.Dominio;
using WCF_TribuTech_Cine.Errores;
using WCF_TribuTech_Cine.Persistencia;

namespace WCF_TribuTech_Cine
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ClienteService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ClienteService.svc o ClienteService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ClienteService : IClienteService
    {
        private ClienteDAO clienteDAO = new ClienteDAO();

        public List<Cliente> ListarClientes()
        {
            return clienteDAO.Index();
        }

        public Cliente CrearCliente(Cliente ClienteObj)
        {
            if (clienteDAO.Show(ClienteObj.Dni) != null)
            {
                throw new FaultException<ClienteErrores>
                    (new ClienteErrores()
                    {
                        Codigo = "101",
                        Descripcion = "El cliente ya se encuentra registrado"
                    },
                        new FaultReason("Error al intentar crear un cliente")
                    );
            }
            return clienteDAO.Store(ClienteObj);
        }

        public Cliente ObtenerCliente(int Dni)
        {
            if (clienteDAO.Show(Dni) == null)
            {
                throw new FaultException<ClienteErrores>
                    (new ClienteErrores()
                    {
                        Codigo = "102",
                        Descripcion = "El cliente no existe"
                    },
                        new FaultReason("Error al intentar obtener un cliente")
                    );
            }
            return clienteDAO.Show(Dni);
        }

        public Cliente ActualizarCliente(Cliente ClienteObj)
        {
            if (ClienteObj.Estado == false)
            {
                throw new FaultException<ClienteErrores>
                    (new ClienteErrores()
                    {
                        Codigo = "103",
                        Descripcion = "No se puede actualizar un cliente deshabilitado"
                    },
                        new FaultReason("Error al intentar actualizar un cliente")
                    );
            }
            return clienteDAO.Update(ClienteObj);
        }

        public void EliminarCliente(int Dni)
        {
            clienteDAO.Delete(Dni);
        }
    }
}
