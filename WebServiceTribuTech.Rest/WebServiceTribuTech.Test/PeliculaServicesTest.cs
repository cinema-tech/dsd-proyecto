﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace WebServiceTribuTech.Test
{
    [TestClass]
    public class PeliculaServicesTest
    {
        [TestMethod]
        public void CrearPelicula_RegistroCorrecto_RetornaPeliculaCreada()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Pelicula peliculaACrear = new Pelicula()
            {
                id = "P0006",
                titulo = "The Irishman 2",
                duracion = "209 min",
                genero = "Gansteres",
                clasificacion = "Mayores de 14",
                productor = "Martin Scorsese",
                pais = "Estados Unidos",
                protagonistas = "Robert De Niro, Al Pacino",
                estado = "D"
            };
            string postdata = js.Serialize(peliculaACrear);
            byte[] data = Encoding.UTF8.GetBytes(postdata);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas");
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json";
            var requeststream = request.GetRequestStream();
            requeststream.Write(data, 0, data.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            Pelicula peliculaCreada = js.Deserialize<Pelicula>(tramaJson);
            Assert.AreEqual("P0006", peliculaCreada.id);
            Assert.AreEqual("The Irishman 2", peliculaCreada.titulo);
            Assert.AreEqual("209 min", peliculaCreada.duracion);
            Assert.AreEqual("Gansteres", peliculaCreada.genero);
            Assert.AreEqual("Mayores de 14", peliculaCreada.clasificacion);
            Assert.AreEqual("Martin Scorsese", peliculaCreada.productor);
            Assert.AreEqual("Estados Unidos", peliculaCreada.pais);
            Assert.AreEqual("Robert De Niro, Al Pacino", peliculaCreada.protagonistas);
            Assert.AreEqual("D", peliculaCreada.estado);
        }

        [TestMethod]
        public void CrearPelicula_PeliculaDuplicada_RetornaMensajeError101()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Pelicula peliculaACrear = new Pelicula()
            {
                id = "P0006",
                titulo = "The Irishman",
                duracion = "209 min",
                genero = "Gansteres",
                clasificacion = "Mayores de 14",
                productor = "Martin Scorsese",
                pais = "Estados Unidos",
                protagonistas = "Robert De Niro, Al Pacino",
                estado = "D"
            };
            string postdata = js.Serialize(peliculaACrear);
            byte[] data = Encoding.UTF8.GetBytes(postdata);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas");
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json";
            var requeststream = request.GetRequestStream();
            requeststream.Write(data, 0, data.Length);
            //HttpWebResponse response = null;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string tramaJson = reader.ReadToEnd();
                Pelicula peliculaCreada = js.Deserialize<Pelicula>(tramaJson);
                Assert.AreEqual("P0006", peliculaCreada.id);
                Assert.AreEqual("The Irishman", peliculaCreada.titulo);
                Assert.AreEqual("209 min", peliculaCreada.duracion);
                Assert.AreEqual("Gansteres", peliculaCreada.genero);
                Assert.AreEqual("Mayores de 14", peliculaCreada.clasificacion);
                Assert.AreEqual("Martin Scorsese", peliculaCreada.productor);
                Assert.AreEqual("Estados Unidos", peliculaCreada.pais);
                Assert.AreEqual("Robert De Niro, Al Pacino", peliculaCreada.protagonistas);
                Assert.AreEqual("D", peliculaCreada.estado);
            }
            catch (WebException e)
            {
                HttpStatusCode codigo = ((HttpWebResponse)e.Response).StatusCode;
                StreamReader reader = new StreamReader(e.Response.GetResponseStream());
                string tramaJson = reader.ReadToEnd();
                DuplicadoException error = js.Deserialize<DuplicadoException>(tramaJson);
                Assert.AreEqual(HttpStatusCode.Conflict, codigo);
                Assert.AreEqual("Pelicula duplicada", error.Descripcion);
            }
        }

        [TestMethod]
        public void ModificarPelicula_PeliculaModifica_RetornaPeliculasModificada()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Pelicula peliculaACrear = new Pelicula()
            {
                id = "P0006",
                titulo = "The Irishman 3",
                duracion = "109 min",
                genero = "Gansteres",
                clasificacion = "Mayores de 14",
                productor = "Martin Scorsese",
                pais = "Nueva Zelanda",
                protagonistas = "Robert De Niro, Al Pacino",
                estado = "ND"
            };
            string postdata = js.Serialize(peliculaACrear);
            byte[] data = Encoding.UTF8.GetBytes(postdata);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas");
            request.Method = "PUT";
            request.ContentLength = data.Length;
            request.ContentType = "application/json";
            var requeststream = request.GetRequestStream();
            requeststream.Write(data, 0, data.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            Pelicula peliculaCreada = js.Deserialize<Pelicula>(tramaJson);
            Assert.AreEqual("P0006", peliculaCreada.id);
            Assert.AreEqual("The Irishman 3", peliculaCreada.titulo);
            Assert.AreEqual("109 min", peliculaCreada.duracion);
            Assert.AreEqual("Gansteres", peliculaCreada.genero);
            Assert.AreEqual("Mayores de 14", peliculaCreada.clasificacion);
            Assert.AreEqual("Martin Scorsese", peliculaCreada.productor);
            Assert.AreEqual("Nueva Zelanda", peliculaCreada.pais);
            Assert.AreEqual("Robert De Niro, Al Pacino", peliculaCreada.protagonistas);
            Assert.AreEqual("ND", peliculaCreada.estado);
        }

        [TestMethod]
        public void ObtenerPelicula_EncuentraPelicula_RetornaPeliculaConsultada()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas/P0002");
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Pelicula peliculaObtenido = js.Deserialize<Pelicula>(tramaJson);
            Assert.AreEqual("P0002", peliculaObtenido.id);
            Assert.AreEqual("The Terminator", peliculaObtenido.titulo);
            Assert.AreEqual("108 min", peliculaObtenido.duracion);
            Assert.AreEqual("Ciencia Ficccion", peliculaObtenido.genero);
            Assert.AreEqual("Mayores 14", peliculaObtenido.clasificacion);
            Assert.AreEqual("Gale Anne Hurd", peliculaObtenido.productor);
            Assert.AreEqual("Estados Unidos", peliculaObtenido.pais);
            Assert.AreEqual("Arnold Schwarzenegger, Linda Hamilton, Michael Biehn", peliculaObtenido.protagonistas);
            Assert.AreEqual("ND", peliculaObtenido.estado);
        }

        [TestMethod]
        public void ListarPeliculas_ListadoPeliculas_RetornaPeliculas()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas");
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<Pelicula> peliculasObtenidas = js.Deserialize<List<Pelicula>>(tramaJson);
            Assert.AreEqual(6, peliculasObtenidas.Count);
        }

        [TestMethod]
        public void EliminarPelicula_EliminarPeliculas_EliminaPeliculas()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas/P0002");
            request.Method = "DELETE";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            HttpWebRequest request2 = (HttpWebRequest)WebRequest.Create("http://localhost:57691/Peliculas.svc/Peliculas/P0002");
            request2.Method = "GET";
            HttpWebResponse response2 = (HttpWebResponse)request2.GetResponse();
            StreamReader reader = new StreamReader(response2.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Pelicula peliculaObtenido = js.Deserialize<Pelicula>(tramaJson);
            Assert.IsNull(peliculaObtenido);

        }

    }
}
