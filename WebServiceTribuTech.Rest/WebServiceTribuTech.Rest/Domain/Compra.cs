﻿using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class Compra
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public decimal total_tickets { get; set; }
        [DataMember]
        public decimal total_costo { get; set; }
        [DataMember]
        public string id_cliente { get; set; }
    }
}