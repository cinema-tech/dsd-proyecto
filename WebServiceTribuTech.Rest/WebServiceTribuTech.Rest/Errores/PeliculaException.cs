﻿using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Errores
{
    [DataContract]
    public class PeliculaException
    {
        [DataMember]
        public int Codigo { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
    }
}