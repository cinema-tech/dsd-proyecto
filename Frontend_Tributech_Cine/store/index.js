import axios from 'axios'

export const state = () => ({
  indexPeliculas: [],
  carteleraPelicula: [],
  salaSeleccionada: [],
  salaAsientos: []
})

export const actions = {
  async obtenerPeliculas({ commit }) {
    try {
      const data = await axios({
        method: 'get',
        url: `http://localhost:3333/peliculas`
      })
      if (data) commit('SET_INDEX_PELICULAS', data.data)
    } catch (e) {
      throw new Error(`Promart api failed, error: ${e}`)
    }
  },

  async detallePelicula({ commit }, peliculaId) {
    try {
      const data = await axios({
        method: 'get',
        url: `http://localhost:3333/cartelera/${peliculaId}`
      })
      if (data) commit('SET_CARTELERA_PELICULA', data.data)
    } catch (e) {
      throw new Error(`Promart api failed, error: ${e}`)
    }
  },

  async obtenerAsientos({ commit }, salaId) {
    try {
      const data = await axios({
        method: 'get',
        url: `http://localhost:3333/asientos/${salaId}`
      })
      if (data) commit('SET_ASIENTOS', data.data)
    } catch (e) {
      throw new Error(`Promart api failed, error: ${e}`)
    }
  },

  async realizarCompra({ commit }, body) {
    try {
      const data = await axios({
        method: 'post',
        url: `http://localhost:3333/compra`,
        data: body
      })
      if (data) return true
    } catch (e) {
      throw new Error(`Promart api failed, error: ${e}`)
    }
  }
}

export const mutations = {
  SET_INDEX_PELICULAS: (state, prop) => {
    state.indexPeliculas = prop
  },
  SET_CARTELERA_PELICULA: (state, prop) => {
    state.carteleraPelicula = prop
  },
  SET_ASIENTOS: (state, prop) => {
    state.salaAsientos = prop
  }
}

export const strict = false
