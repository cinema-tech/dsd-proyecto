﻿using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class TipoSala
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string descripcion { get; set; }
    }
}