﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF_TribuTech_Cine.Dominio
{
    [DataContract]
    public class Cliente
    {
        [DataMember]
        public int Id { get; }
        [DataMember]
        public int Dni { get; set; }
        [DataMember]
        public string Nombres { get; set; }
        [DataMember]
        public string ApellidoPaterno { get; set; }
        [DataMember]
        public string ApellidoMaterno { get; set; }
        [DataMember]
        public DateTime FechaNacimiento { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public bool Estado { get; }
    }
}