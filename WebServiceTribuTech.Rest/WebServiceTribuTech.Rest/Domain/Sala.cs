﻿using System;
using System.Runtime.Serialization;

namespace WebServiceTribuTech.Rest.Domain
{
    [DataContract]
    public class Sala
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int nombre { get; set; }
        [DataMember]
        public string codigo { get; set; }
        [DataMember]
        public string id_tipo_sala { get; set; }
        [DataMember]
        public string cantidad_asientos { get; set; }
    }
}